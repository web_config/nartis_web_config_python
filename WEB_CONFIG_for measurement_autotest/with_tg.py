import json
import time
import asyncio
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

from telegram import Bot
from telegram import InputFile

try:
    req = int(input("ENTER NUMBER OF REQUESTS: "))  # Количество запросов
    sec = int(input("ENTER TIME INTERVAL BETWEEN REQUESTS,(sec.):"))  # Время между запросами
except ValueError:
    print("TRY AGAIN.YOU HAVE TO ENTER INTEGER!!!", "\n", "RESTART THE PROGRAM!!!")
    exit()

try:
    telegram= input("IF YOU WANT TO SEND FILE TO TELEGRAM TYPE >YES< : ") # Нужно ли отправлять файл в телеграм
except ValueError:
    print("TRY AGAIN.YOU HAVE TO ENTER Y or N !!!", "\n", "RESTART THE PROGRAM!!!")
    exit()

driver = webdriver.Firefox()

#Создание(очистка) файла data.json
with open('data.json', 'w') as f:
    json.dump('.....RESULTS OF MEASUREMENT......', f)
    f.write('\n')

def measure(): # Выполнения запросов через selenium
    for _ in range(req):
        try:
            WebDriverWait(driver, 1)
            refresh = driver.find_element(By.CLASS_NAME, 'tooltips')
            ActionChains(driver).move_to_element(refresh).perform()
            ActionChains(driver).click().perform()
            time.sleep(sec)

            # Параметры для вывода
            date_of = driver.find_element(By.XPATH, '//*[contains(text(), "' + time.strftime("%d.%m.%Y") + '")]')
            temperature = driver.find_element(By.CSS_SELECTOR, "tbody tr:nth-child(29) td:last-child")
            P_kvt = driver.find_element(By.CSS_SELECTOR, "tbody tr:nth-child(3) td:last-child")
            Q_kvar = driver.find_element(By.CSS_SELECTOR, "tbody tr:nth-child(4) td:last-child")
            S_kva = driver.find_element(By.CSS_SELECTOR, "tbody tr:nth-child(5) td:last-child")

            print("|", date_of.text, "-------", temperature.text, "----------------", P_kvt.text,
                  "-----------------------", Q_kvar.text, "------------------------", S_kva.text, "------|")
            write_to_json(date_of, temperature, P_kvt, Q_kvar, S_kva)

        except NoSuchElementException:
            print("Element not found on the page.")
            break
def write_to_json(date_of, temperature, P_kvt, Q_kvar, S_kva):# запись в файл
    with open('data.json', 'a') as file:
        data = {
            'Date_Time': date_of.text,
            'Temperature': temperature.text,
            'Active power, kWt': P_kvt.text,
            'Reactive power, kVAr': Q_kvar.text,
            'Full power, kVA': S_kva.text
        }
        json.dump(data, file)
        file.write('\n')
async def tg(): # Отправка в телеграм
    bot_token = '6767941542:AAFmbN14X3-2q3ivDKUOpbluTYCurKri21c'
    chat_id = '386631987'
    file_path = 'data.json'
    bot = Bot(bot_token)
    input_file = InputFile(file_path)
    with open(file_path, 'rb') as f:
        input_file = InputFile(f)
    await bot.send_document(chat_id, input_file)

driver.get("http://localhost:5000/NetworkSettings")
print(""" ---------------------------------------------------------------------------------------------------------------------------------
|     DATE/TIME            TEMPERATURE            ACTIVE POWER (kWt)            REACTIVE POWER (kVAr)            FULL POWER (kVA) |
""")
measure()
print(
    "-----------------------------------------------------------------------------------------------------------------------------------")
if telegram.strip().lower() == 'yes':
    asyncio.run(tg())
else:
    pass
